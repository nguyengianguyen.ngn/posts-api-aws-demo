namespace Posts.API.Contracts.Responses;

public class MutationResultResponse
{
    public bool Result { get; set; }
}
