using System.Text.Json.Serialization;

namespace Posts.API.Contracts.Data;

public class PostDto
{
    [JsonPropertyName("id")]
    public string Id { get; init; } = default!;

    [JsonPropertyName("name")]
    public string Name { get; set; } = default!;

    [JsonPropertyName("content")]
    public string Content { get; set; } = default!;

    [JsonPropertyName("updated")]
    public string? Updated { get; set; } = default;
}
