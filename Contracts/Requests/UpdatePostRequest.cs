public class UpdatePostRequest
{
    public string Name { get; set; } = default!;
    public string Content { get; set; } = default!;
}
