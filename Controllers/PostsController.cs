﻿using Microsoft.AspNetCore.Mvc;
using Posts.API.Contracts.Data;
using Posts.API.Contracts.Responses;
using Posts.API.Repositories;

namespace Posts.API.Controllers;

[Route("api/[controller]")]
public class PostsController : ControllerBase
{
    private readonly IPostRepository _postRepository;
    private readonly ILogger<PostsController> _logger;

    public PostsController(IPostRepository postRepository, ILogger<PostsController> logger)
    {
        _postRepository = postRepository;
        _logger = logger;
    }

    // GET api/posts
    [HttpGet]
    public async Task<ICollection<PostDto>> Get()
    {
        ICollection<PostDto>? postDtos = await _postRepository.GetAsync();

        if (postDtos == null)
        {
            return new List<PostDto>();
        }

        return postDtos;
    }

    // GET api/posts/5
    [HttpGet("{id:guid}")]
    public async Task<PostDto?> GetAsync([FromRoute] Guid id)
    {
        PostDto? postDto = await _postRepository.GetAsync(id);

        return postDto;
    }

    // POST api/posts
    [HttpPost]
    public async Task<MutationResultResponse> Post([FromBody] CreatePostRequest request)
    {
        var post = new PostDto
        {
            Id = Guid.NewGuid().ToString(),
            Name = request.Name,
            Content = request.Content
        };

        _logger.LogInformation("Creating new post");

        bool result = await _postRepository.CreateAsync(post);

        return new MutationResultResponse { Result = result };
    }

    // // PUT api/posts/5
    [HttpPut("{id:guid}")]
    public async Task<MutationResultResponse> Put(
        [FromRoute] Guid id,
        [FromBody] UpdatePostRequest request
    )
    {
        var post = new PostDto
        {
            Id = id.ToString(),
            Name = request.Name,
            Content = request.Content,
        };

        bool result = await _postRepository.UpdateAsync(post);

        return new MutationResultResponse { Result = result };
    }

    // DELETE api/posts/5
    [HttpDelete("{id:guid}")]
    public async Task<MutationResultResponse> Delete([FromRoute] Guid id)
    {
        bool result = await _postRepository.DeleteAsync(id);

        return new MutationResultResponse { Result = result };
    }
}
