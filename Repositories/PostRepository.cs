using System.Net;
using System.Text.Json;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Posts.API.Contracts.Data;

namespace Posts.API.Repositories;

public class PostRepository : IPostRepository
{
    private readonly IAmazonDynamoDB _dynamoDB;

    private readonly string _tableName = "post"; // or could read from configuration instead

    public PostRepository(IAmazonDynamoDB dynamoDB)
    {
        _dynamoDB = dynamoDB;
    }

    public async Task<bool> CreateAsync(PostDto post)
    {
        string postAsJson = JsonSerializer.Serialize(post);

        Document postAsDocument = Document.FromJson(postAsJson);

        Dictionary<string, AttributeValue> postAsAttributes = postAsDocument.ToAttributeMap();

        var createPostRequest = new PutItemRequest
        {
            TableName = _tableName,
            Item = postAsAttributes
        };

        PutItemResponse response = await _dynamoDB.PutItemAsync(createPostRequest);

        return response.HttpStatusCode == HttpStatusCode.OK;
    }

    public async Task<bool> DeleteAsync(Guid id)
    {
        var deletePostRequest = new DeleteItemRequest
        {
            TableName = _tableName,
            Key = new Dictionary<string, AttributeValue>
            {
                {
                    "id",
                    new AttributeValue { S = id.ToString() }
                }
            }
        };

        DeleteItemResponse response = await _dynamoDB.DeleteItemAsync(deletePostRequest);

        return response.HttpStatusCode == HttpStatusCode.OK;
    }

    public async Task<ICollection<PostDto>?> GetAsync()
    {
        var conditions = new List<ScanCondition>();

        // name là reserved keyword nên phải sử dụng Expression Attribute Name để lấy được cột name
        // prefix # để đánh dấu từ khóa là placeholder chứ không phải tên của cột
        var scanRequest = new ScanRequest
        {
            TableName = _tableName,
            ExpressionAttributeNames = new Dictionary<string, string> { { "#post_name", "name" } },
            ProjectionExpression = "id, #post_name, content, updated"
        };

        ScanResponse response = await _dynamoDB.ScanAsync(scanRequest);

        IEnumerable<PostDto> postDtos = response.Items.Select(
            x =>
                new PostDto
                {
                    Id = x["id"].S.ToString(),
                    Name = x["name"].S.ToString(),
                    Content = x["content"].S.ToString(),
                    Updated = x.ContainsKey("updated") ? x["updated"].S.ToString() : null
                }
        );

        return postDtos.ToList();
    }

    public async Task<PostDto?> GetAsync(Guid id)
    {
        // GetItemRequest nhanh hơn QueryRequest do duyệt theo key HASH.
        var getPostRequest = new GetItemRequest
        {
            TableName = _tableName,
            Key = new Dictionary<string, AttributeValue>
            {
                {
                    "id",
                    new AttributeValue { S = id.ToString() }
                }
            }
        };

        GetItemResponse response = await _dynamoDB.GetItemAsync(getPostRequest);

        if (response.Item.Count == 0)
        {
            return null;
        }

        Document postAsDocument = Document.FromAttributeMap(response.Item);

        PostDto? post = JsonSerializer.Deserialize<PostDto>(postAsDocument.ToJson());

        return post;
    }

    public async Task<bool> UpdateAsync(PostDto post)
    {
        var updatePostRequest = new UpdateItemRequest
        {
            TableName = _tableName,
            Key = new Dictionary<string, AttributeValue>
            {
                {
                    "id",
                    new AttributeValue { S = post.Id }
                }
            },
            ExpressionAttributeNames = new Dictionary<string, string> { { "#post_name", "name" }, },
            ExpressionAttributeValues = new Dictionary<string, AttributeValue>
            {
                {
                    ":new_name",
                    new AttributeValue { S = post.Name }
                },
                {
                    ":new_content",
                    new AttributeValue { S = post.Content }
                },
                {
                    ":new_updated",
                    new AttributeValue { S = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString() }
                }
            },
            UpdateExpression =
                "SET #post_name = :new_name, content = :new_content, updated = :new_updated"
        };

        UpdateItemResponse response = await _dynamoDB.UpdateItemAsync(updatePostRequest);

        return response.HttpStatusCode == HttpStatusCode.OK;
    }
}
