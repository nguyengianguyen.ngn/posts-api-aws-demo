using Posts.API.Contracts.Data;

namespace Posts.API.Repositories;

public interface IPostRepository
{
    Task<bool> CreateAsync(PostDto post);
    Task<ICollection<PostDto>?> GetAsync();
    Task<PostDto?> GetAsync(Guid id);
    Task<bool> DeleteAsync(Guid id);
    Task<bool> UpdateAsync(PostDto post);
}
